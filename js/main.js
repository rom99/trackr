function main() {
	console.log('started...')

	var x = 0, 
		y = 0, 
		log = [],
		lastLog = [x, y],
		xDisplay = el('x-val'),	
		yDisplay = el('y-val'),
		trackr = el('trackr'),
		trackrContext = trackr.getContext('2d'),
		canvasSize = [trackr.width, trackr.height];

	console.log('canvas size', canvasSize);
	console.log('body width', document.body.clientWidth);

	function on(element, event, callback) {
		element.addEventListener(event, callback);
	}

	function el(id) {
		return document.getElementById(id);
	}

	function handleMouseMove(evt) {
		x = evt.x;
		y = evt.y;
	}

	var flash = (function flash() {
		
		function getColor(logItem) {
			var scale = proportional(logItem, [10, 10]);
			var r = (5 + Math.floor(scale[0])).toString(16);
			var g = (5 + Math.floor(scale[1])).toString(16);
			var b = 1;
			var color = '#' + r + g + b;
			return color;
		}

		return function(logItem) {
			trackrContext.beginPath();
			trackrContext.fillStyle = getColor(logItem);
			trackrContext.rect(0, 0, canvasSize[0], canvasSize[1]);
			trackrContext.fill();		
		}
	})()

	function clearCanvas(logItem) {
		trackrContext.clearRect(0, 0, canvasSize[0], canvasSize[1]);
		flash(logItem);
	}

	function markPoint(context, point) {
		var x = point[0], y = point[1];
		context.beginPath();
		context.arc(x, y, 2, 0, 2 * Math.PI, false);
		context.fillStyle = 'green';
		context.fill();
		context.lineWidth = 1;
		context.strokeStyle = '#003300';
		context.stroke();
	}

	function getScreenRatio(scale) {
		return [scale[0] / document.body.clientWidth, scale[1] / document.body.clientHeight	];
	}

	function proportional(point, scale) {
		var ratio = getScreenRatio(scale);
		return [point[0] * ratio[0], point[1] * ratio[1]];
	}

	function update(logItem) {
		xDisplay.textContent = logItem[0];
		yDisplay.textContent = logItem[1];
		clearCanvas(logItem);
		markPoint(trackrContext, proportional(logItem, canvasSize));
	}

	function logMovement() {
		var newLog = [x, y];
		if(newLog[0] === lastLog[0] && newLog[1] === lastLog[1]) {
			return;
		}
		update(newLog);
		lastLog = newLog;
		log.push(newLog);
	}

	function replayLog() {
		var idx = 0;
		function replay() {
			var logItem = log[idx];
			console.log('replaying ', logItem);
			update(log[idx]);
			idx = (idx + 1) % (log.length - 1);
		}

		var replayer = window.setInterval(replay, 60);
	}

	console.log('Logging...');
	var logger = window.setInterval(logMovement, 60);

	var doReplay = window.setTimeout(function() {
		console.log('Replaying...', log)
		window.clearInterval(logger);
		replayLog();
	}, 5000);

	on(document.body, 'mousemove', handleMouseMove);
}

main();