# Trackr #

A simple web page that records mouse movements and plays them back. It maps the mouse position to a color with the x and y position controlling red and green colour channels respectively. Pure JavaScript (no frameworks used).  No real purpose in mind, just for fun.